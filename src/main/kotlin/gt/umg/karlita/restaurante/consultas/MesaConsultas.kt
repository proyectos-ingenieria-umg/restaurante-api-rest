package gt.umg.karlita.restaurante.consultas

import gt.umg.karlita.restaurante.models.MesaModel
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Repository
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Repository
class MesaConsultas {

    @PersistenceContext
    @Qualifier
    private lateinit var entityManager: EntityManager

    fun insertarMesa(mesa: MesaModel): Int {

        val sQuery = "insert into mesa(activo, creado_por, fecha_creacion, cantidad_maxima_personas, numero_mesa, mesa_estado_id) " +
                " values(true, :creado_por, current_timestamp, :cantidad_maxima_personas, :numero_mesa, :mesa_estado_id) RETURNING id"

        val query = entityManager.createNativeQuery(sQuery)

        query.setParameter("creado_por", mesa.creadoPor)
                .setParameter("cantidad_maxima_personas", mesa.cantidadMaximaPersonas)
                .setParameter("numero_mesa", mesa.numeroMesa)
                .setParameter("mesa_estado_id", mesa.mesaEstadoId)

        val resultado = query.singleResult

        return resultado as Int
    }

    fun actualizarMesa(mesa: MesaModel) {

        val sQuery = "update mesa " +
                "        set activo = :activo, " +
                "            cantidad_maxima_personas = :cantidad_maxima_personas, " +
                "            numero_mesa = :numero_mesa," +
                "            mesa_estado_id = :mesa_estado_id " +
                "       where id = :id"

        val query = entityManager.createNativeQuery(sQuery)

        query.setParameter("cantidad_maxima_personas", mesa.cantidadMaximaPersonas)
                .setParameter("numero_mesa", mesa.numeroMesa)
                .setParameter("mesa_estado_id", mesa.mesaEstadoId)
                .setParameter("id", mesa.id)

        query.executeUpdate()
    }

    fun obtenerMesas(): List<MesaModel> {
        val sQuery = "select t0.id, t0.creado_por, t0.fecha_creacion, t0.cantidad_maxima_personas, t0.numero_mesa, t0.mesa_estado_id, t1.mesa_estado_nombre" +
                "      from mesa as t0 " +
                "      join mesa_estado as t1 on t1.id = t0.mesa_estado_id " +
                "      where t0.activo = true "

        val query = entityManager.createNativeQuery(sQuery)

        return query
                .resultList
                .map { item ->
                    val data = item as Array<*>

                    return@map MesaModel().apply {
                        this.id = data[0] as Int
                        this.creadoPor = data[1] as String
                        this.cantidadMaximaPersonas = data[3] as Int
                        this.numeroMesa = data[4] as Int
                        this.mesaEstadoId = data[5] as Int
                        this.mesaEstadoNombre = data[6] as String
                    }
                }
                .toList()
    }


}
