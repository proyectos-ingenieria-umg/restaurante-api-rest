package gt.umg.karlita.restaurante.consultas

import gt.umg.karlita.restaurante.models.MesaEstadoModel
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Repository
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Repository
class MesaEstadoConsultas {

    @PersistenceContext
    @Qualifier
    private lateinit var entityManager: EntityManager

    fun obtenerEstadoPorNombre(nombreEstado: String): MesaEstadoModel? {

        val sQuery = "select id, mesa_estado_nombre, fecha_creacion, creado_por, activo " +
                "      from mesa_estado " +
                "      where mesa_estado_nombre = :nombre_estado"

        val query = entityManager.createNativeQuery(sQuery)

        query.setParameter("nombre_estado", nombreEstado)

        val resultado = query
                .resultList
                .map { item ->
                    val data = item as Array<*>

                    return@map MesaEstadoModel().apply {
                        this.id = data[0] as Int
                        this.mesaEstadoNombre = data[1] as String
                        this.creadoPor = data[3] as String
                        this.activo = data[4] as Boolean
                    }
                }

        return when {
            resultado.isNotEmpty() -> resultado[0]
            else -> null
        }
    }


}
