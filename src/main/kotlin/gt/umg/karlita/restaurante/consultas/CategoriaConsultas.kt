package gt.umg.karlita.restaurante.consultas

import gt.umg.karlita.restaurante.models.CategoriaModel
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Repository
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Repository
class CategoriaConsultas {

    @PersistenceContext
    @Qualifier
    private lateinit var entityManager: EntityManager

    fun crearCategoria(categoria: CategoriaModel): Int {

        val sQuery = "insert into categoria(activo, creado_por, fecha_creacion, categoria_nombre) " +
                "      values(true, :creado_por, current_timestamp, :categoria_nombre) returning id"

        val query = entityManager.createNativeQuery(sQuery)

        query.setParameter("creado_por", categoria.creadoPor)
                .setParameter("categoria_nombre", categoria.categoriaNombre)

        val resultado = query.singleResult

        return resultado as Int
    }

    fun actualizarCategoria(categoria: CategoriaModel) {
        val sQuery = "update categoria set categoria_nombre = :categoria_nombre where id = :id"

        val query = entityManager.createNativeQuery(sQuery)

        query.setParameter("categoria_nombre", categoria.categoriaNombre)
                .setParameter("id", categoria.id)

        query.executeUpdate()
    }

    fun obtenerCategorias(): List<CategoriaModel> {
        val sQuery = "select id, creado_por, fecha_creacion, categoria_nombre " +
                "      from categoria " +
                "      where activo = true " +
                "      order by categoria_nombre asc"

        val query = entityManager.createNativeQuery(sQuery)

        return query
                .resultList
                .map { item ->
                    val data = item as Array<*>

                    return@map CategoriaModel().apply {
                        this.id = data[0] as Int
                        this.creadoPor = data[1] as String
                        this.categoriaNombre = data[3] as String
                    }
                }
                .toList()
    }

}
