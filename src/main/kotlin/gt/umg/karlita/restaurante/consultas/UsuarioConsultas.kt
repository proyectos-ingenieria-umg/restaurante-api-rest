package gt.umg.karlita.restaurante.consultas

import gt.umg.karlita.restaurante.models.UsuarioModel
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Repository
import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Repository
class UsuarioConsultas {

    @PersistenceContext
    @Qualifier
    private lateinit var entityManager: EntityManager

    fun buscarUsuarioPorEmail(email: String): UsuarioModel? {

        val sQuery = "select id, " +
                "        activo, " +
                "        usuario_email, " +
                "        usuario_nombre, " +
                "        usuario_password, " +
                "        fecha_creacion, " +
                "        creado_por " +
                "      from usuario " +
                "       where usuario_email = :email"

        val query = entityManager.createNativeQuery(sQuery)

        query.setParameter("email", email)

        val resultado = query
                .resultList
                .map { item ->
                    val data = item as Array<*>

                    return@map UsuarioModel().apply {
                        this.id = data[0] as Int
                        this.activo = data[1] as Boolean
                        this.usuarioEmail = data[2] as String
                        this.usuarioNombre = data[3] as String
                        this.usuarioPassword = data[4] as String
                        this.creadoPor = data[6] as String
                    }
                }

        return when {
            resultado.isNotEmpty() -> resultado[0]
            else -> null
        }

    }

}
