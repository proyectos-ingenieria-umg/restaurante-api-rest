package gt.umg.karlita.restaurante.utilidades

import com.fasterxml.jackson.databind.ObjectMapper
import gt.umg.karlita.restaurante.models.TokenInfoModel
import java.util.*

class TokenUtils {

    private val mapper = ObjectMapper()

    fun crearToken(usuarioId: Int, nombre: String, email: String, rol: String): String {
        val tokenData = TokenInfoModel()
        tokenData.usuarioId = usuarioId
        tokenData.nombre = nombre
        tokenData.email = email
        tokenData.rol = rol

        val tokenDataJSON: String = mapper.writeValueAsString(tokenData)

        return Base64.getEncoder().encodeToString(tokenDataJSON.toByteArray())
    }

    fun validarToken(authorization: String): TokenInfoModel? {
        if (authorization.isEmpty()) {
            return null
        }

        val headerParts = authorization.split(' ')

        if (headerParts.isEmpty() || headerParts[0] != "Basic") {
            return null
        }

        val token = headerParts[1]

        val decodedBytes = Base64.getDecoder().decode(token)

        val claims = String(decodedBytes)

        return mapper.readValue(claims, TokenInfoModel::class.java)
    }

}
