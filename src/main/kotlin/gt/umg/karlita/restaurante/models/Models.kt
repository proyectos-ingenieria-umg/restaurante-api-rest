package gt.umg.karlita.restaurante.models

import java.math.BigDecimal
import java.time.LocalDateTime

class LoginRespuesta {
    var loginExitoso: Boolean = false
    var error: String = ""
    var token: String = ""
}

class LoginSolicitud {
    var email: String = ""
    var password: String = ""
}

class TokenInfoModel {
    var usuarioId: Int = 0
    var email: String = ""
    var nombre: String = ""
    var rol: String = ""
}

class UsuarioModel {
    var id: Int = 0
    var usuarioEmail: String = ""
    var usuarioNombre: String = ""
    var usuarioPassword: String = ""
    var fechaCreacion: String = ""
    var creadoPor: String = ""
    var activo: Boolean = true
}

class MesaModel {
    var id: Int = 0
    var numeroMesa: Int = 0
    var cantidadMaximaPersonas: Int = 0
    var mesaEstadoId: Int = 0
    var mesaEstadoNombre: String = ""
    var fechaCreacion: LocalDateTime = LocalDateTime.now()
    var creadoPor: String = ""
    var activo: Boolean = true
}

class MesaEstadoModel {
    var id: Int = 0
    var mesaEstadoNombre: String = ""
    var fechaCreacion: LocalDateTime = LocalDateTime.now()
    var creadoPor: String = ""
    var activo: Boolean = true
}

class CategoriaModel {
    var id: Int = 0
    var activo: Boolean = true
    var creadoPor: String = ""
    var fechaCreacion: LocalDateTime = LocalDateTime.now()
    var categoriaNombre: String = ""
}

class ProductoModel {
    var id: Int = 0
    var activo: Boolean = true
    var creadoPor: String = ""
    var fechaCreacion: LocalDateTime = LocalDateTime.now()
    var precio: BigDecimal = BigDecimal.ZERO
    var productoDescripcion: String = ""
    var productoNombre: String = ""
    var productoTipoId: Int = 0
    var categoriaId: Int = 0
}

class ProductoTipoModel {
    var id: Int = 0
    var activo: Boolean = true
    var creadoPor: String = ""
    var fechaCreacion: LocalDateTime = LocalDateTime.now()
    var productoTipoNombre: String = ""
}
