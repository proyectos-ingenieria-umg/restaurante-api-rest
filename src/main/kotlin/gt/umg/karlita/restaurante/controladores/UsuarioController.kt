package gt.umg.karlita.restaurante.controladores

import gt.umg.karlita.restaurante.consultas.UsuarioConsultas
import gt.umg.karlita.restaurante.exceptions.ErrorInternoException
import gt.umg.karlita.restaurante.models.LoginRespuesta
import gt.umg.karlita.restaurante.models.LoginSolicitud
import gt.umg.karlita.restaurante.models.UsuarioModel
import gt.umg.karlita.restaurante.utilidades.TokenUtils
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
@RequestMapping(value = ["/api/usuario"])
class UsuarioController {

    private val log = LoggerFactory.getLogger(javaClass)

    @Autowired
    private lateinit var usuarioConsultas: UsuarioConsultas

    @PostMapping(value = ["/login"])
    fun login(
            @RequestBody solicitud: LoginSolicitud
    ): ResponseEntity<LoginRespuesta> {
        val email = solicitud.email
        val password = solicitud.password

        // 1. Verificamos los datos requeridos
        if (email.isBlank() && password.isBlank()) {
            throw ErrorInternoException("Email y password son requeridos")
        }

        // 2. Buscamos el usuario por su email
        val usuario: UsuarioModel = usuarioConsultas.buscarUsuarioPorEmail(email)
                ?: throw ErrorInternoException("Usuario o password incorrectos")

        // 3. Codificamos el password a base64 para compararlo con el password del usuario
        val passwordCodificado: String = Base64.getEncoder().encodeToString(password.toByteArray())

        log.info("Password codificado {}", passwordCodificado)

        // 4. Hacemos la comparacion de passwords
        // Si el password es correcto, realizamos login, creamos token y retornamos la respuesta
        // de lo contrario retornamos un error
        if (passwordCodificado == usuario.usuarioPassword) {
            val respuesta = LoginRespuesta()

            respuesta.loginExitoso = true
            respuesta.token = TokenUtils().crearToken(usuario.id, usuario.usuarioNombre, usuario.usuarioEmail, "CAJERO");

            return ResponseEntity.ok(respuesta)
        } else {
            throw ErrorInternoException("Usuario o password incorrectos")
        }

    }

}
