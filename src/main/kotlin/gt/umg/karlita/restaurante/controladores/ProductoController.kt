package gt.umg.karlita.restaurante.controladores

import gt.umg.karlita.restaurante.consultas.ProductoConsultas
import gt.umg.karlita.restaurante.exceptions.AccesoNoAutorizadoException
import gt.umg.karlita.restaurante.models.ProductoModel
import gt.umg.karlita.restaurante.utilidades.TokenUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/api/producto"])
class ProductoController {

    @Autowired
    private lateinit var productoConsultas: ProductoConsultas

    @PostMapping
    fun crearProducto(
            @RequestHeader(value = "authorization", defaultValue = "") authorization: String,
            @RequestBody producto: ProductoModel
    ): ResponseEntity<Int> {

        val tokenInfo = TokenUtils()
                .validarToken(authorization)
                ?: throw AccesoNoAutorizadoException()

        producto.creadoPor = tokenInfo.email

        val id = productoConsultas.crearProducto(producto)

        return ResponseEntity.status(HttpStatus.CREATED).body(id)

    }

    @PutMapping
    @Transactional(readOnly = false, rollbackFor = [Exception::class])
    fun actualizarProducto(
            @RequestHeader(value = "authorization", defaultValue = "") authorization: String,
            @RequestBody producto: ProductoModel
    ): ResponseEntity<Boolean> {

        val tokenInfo = TokenUtils()
                .validarToken(authorization)
                ?: throw AccesoNoAutorizadoException()

        productoConsultas.actualizarProducto(producto)

        return ResponseEntity.ok(true)
    }

    @GetMapping
    fun obtenerProductosPorCategoria(
            @RequestParam(value = "categoriaId") categoriaId: Int
    ): ResponseEntity<List<ProductoModel>> {
        val productos = productoConsultas.obtenerProductosPorCategoria(categoriaId)

        return ResponseEntity.ok(productos)
    }

}
