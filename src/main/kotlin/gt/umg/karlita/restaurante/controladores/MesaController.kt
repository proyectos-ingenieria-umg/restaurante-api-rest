package gt.umg.karlita.restaurante.controladores

import gt.umg.karlita.restaurante.consultas.MesaConsultas
import gt.umg.karlita.restaurante.consultas.MesaEstadoConsultas
import gt.umg.karlita.restaurante.exceptions.AccesoNoAutorizadoException
import gt.umg.karlita.restaurante.exceptions.ErrorInternoException
import gt.umg.karlita.restaurante.models.MesaModel
import gt.umg.karlita.restaurante.utilidades.TokenUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestHeader
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/api/mesa"])
class MesaController {

    @Autowired
    private lateinit var mesaConsultas: MesaConsultas

    @Autowired
    private lateinit var mesaEstadoConsultas: MesaEstadoConsultas

    @PostMapping
    fun crearMesa(
            @RequestHeader(value = "authorization", defaultValue = "") authorization: String,
            @RequestBody mesa: MesaModel
    ): ResponseEntity<Int> {

        val tokenInfo = TokenUtils()
                .validarToken(authorization)
                ?: throw AccesoNoAutorizadoException()

        val mesaEstado = mesaEstadoConsultas
                .obtenerEstadoPorNombre("Libre")
                ?: throw ErrorInternoException("El estado 'Libre' no existe en la base de datos")

        mesa.creadoPor = tokenInfo.email
        mesa.mesaEstadoId = mesaEstado.id

        val id = mesaConsultas.insertarMesa(mesa)

        return ResponseEntity.status(HttpStatus.CREATED).body(id)
    }

    @PutMapping
    fun actualizarMesa(
            @RequestHeader(value = "authorization", defaultValue = "") authorization: String,
            @RequestBody mesa: MesaModel
    ): ResponseEntity<Boolean> {
        val tokenInfo = TokenUtils()
                .validarToken(authorization)
                ?: throw AccesoNoAutorizadoException()

        mesaConsultas.actualizarMesa(mesa)

        return ResponseEntity.ok(true)
    }

    @GetMapping
    fun obtenerMesas(): ResponseEntity<List<MesaModel>> {
        val mesas = mesaConsultas.obtenerMesas()

        return ResponseEntity.ok(mesas)
    }

}
